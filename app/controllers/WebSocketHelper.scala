package controllers

import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{Flow, Sink, Source, SourceQueueWithComplete}
import play.api.mvc.WebSocket

import scala.concurrent.ExecutionContext

object WebSocketHelper {

  def createWebSocket[In, Out](
          queueReady: SourceQueueWithComplete[Out] => Unit,
          handleMsg: In => Unit,
          whenDone: SourceQueueWithComplete[Out] => Unit
  )(implicit ec: ExecutionContext, t: WebSocket.MessageFlowTransformer[In, Out]):WebSocket = {

    WebSocket.accept[In, Out] { _ =>
      // Our source is going to be an asynchronous queue we can push messages to with 'out.offer(msg)'. This is going to
      // be the "source" of our flow. A complication is it doesn't create the queue immediately - this just says
      // *when* this source is connected to a flow, create a queue. So we don't have a queue yet.
      val outSource = Source.queue[Out](50, OverflowStrategy.backpressure)

      // Create a "sink" that describes what we want to do with each message. Here, I'm just going to count the characters
      // and echo it straight back out on the "out" queue.
      val in = Sink.foreach[In](handleMsg)

      // This defines a "flow" -- something that has an input and an output.
      // "Coupled" means that if the input closes, the output will close
      // "Mat" means "materialised" -- ie, it'll give us the output queue that gets created and the Future that will
      // complete when the flow is done.
      Flow.fromSinkAndSourceCoupledMat(in, outSource) { case (done, out) =>
        queueReady(out)
        done.onComplete { _ => whenDone(out) }
      }
    }
  }



}
