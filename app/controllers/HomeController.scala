package controllers

import java.lang.ProcessBuilder.Redirect

import javax.inject._
import play.api._
import play.api.mvc._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {



  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    Redirect("assets/lavaclient/index.html")
  }

  val csvFile = "logins.csv"

  def csv = Action.async { implicit request: Request[AnyContent] =>

    import java.io._
    import scala.util.Try
    import scala.concurrent.Future

    Future.fromTry(Try {
      val fw = new FileWriter(csvFile, true)
      fw.write(request.body.asText + "\n")
      fw.flush()
      fw.close()

      Ok("")
    })

  }


}
