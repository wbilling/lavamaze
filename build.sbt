name := """play-scala-seed"""
organization := "cosc360"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"

resolvers += "Hopper releases" at "http://hopper.une.edu.au/artifactory/libs-release/"

resolvers += "Hopper snapshots" at "http://hopper.une.edu.au/artifactory/libs-snapshot"

libraryDependencies += guice

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.10"

libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "cosc360.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "cosc360.binders._"
